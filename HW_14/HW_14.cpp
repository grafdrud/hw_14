﻿#include <iostream>
#include <iomanip>
#include <string>

int main()
{
    std::cout << "Enter your nickname: ";
    std::string nickname;
    std::getline(std::cin, nickname);

    std::cout << "Your nickname: " << nickname << "\n";
    std::cout << "Nickname length = " << nickname.length() << "\n";
    std::cout << "First symbol: " << nickname[0] << "\n";
    std::cout << "Last symbol: " << nickname[nickname.size() - 1] << "\n";
}
